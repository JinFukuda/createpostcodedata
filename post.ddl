create table post(
  post_code varchar(7) not null primary key,
  pref varchar(100),
  city varchar(100),
  address varchar(100),
  pref_kana varchar(100),
  city_kana varchar(100),
  address_kana varchar(100)
)
;