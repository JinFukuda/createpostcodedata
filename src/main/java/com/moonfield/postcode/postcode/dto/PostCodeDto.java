package com.moonfield.postcode.postcode.dto;

import lombok.Data;

@Data
public class PostCodeDto {
	
	// 郵便番号
	private String postcode;
	
	// 県
	private String prefecture;
	
	// 市区町村
	private String city;
	
	// 番地
	private String address;
	
	// 県(カナ)
	private String prefectureKana;
	
	// 市区町村(カナ)
	private String cityKana;
	
	// 番地(カナ)
	private String addressKana;
	

}
