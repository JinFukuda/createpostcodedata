package com.moonfield.postcode.postcode.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.moonfield.postcode.exception.DaoException;
import com.moonfield.postcode.postcode.dto.PostCodeDto;

public class PostCodeDao extends BaseDao {
	
	public int insertPrefCode(PostCodeDto dto) throws DaoException{
		
		init();
		PreparedStatement pstmt = null;
		
		try {
			
			pstmt = conn.prepareStatement(" INSERT INTO post ( post_code, pref, city, address, pref_kana, city_kana, address_kana) values (?, ?, ?, ?, ?, ?, ?)");
			pstmt.setString(1, dto.getPostcode());
			pstmt.setString(2, dto.getPrefecture());
			pstmt.setString(3, dto.getCity());
			pstmt.setString(4, dto.getAddress());
			pstmt.setString(5, dto.getPrefectureKana());
			pstmt.setString(6, dto.getCityKana());
			pstmt.setString(7, dto.getAddressKana());
			
			return pstmt.executeUpdate();
			
		}catch(SQLException e) {
			e.printStackTrace();
			throw new DaoException();
		}finally {
			destroy();
		}
		
	}
	
	public boolean selectPostCode(String postcode) throws DaoException{
		
		init();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			
			pstmt = conn.prepareStatement(" SELECT * from post where post_code = ? ");
			pstmt.setString(1, postcode);
			
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				return true;
			}
			
			return false;
		}catch(SQLException e) {
			e.printStackTrace();
			throw new DaoException();
		}finally {
			destroy();
		}
		
	}

}
