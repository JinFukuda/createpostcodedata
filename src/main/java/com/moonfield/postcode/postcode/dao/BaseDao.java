package com.moonfield.postcode.postcode.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.moonfield.postcode.exception.DaoException;

public class BaseDao {
	
	private static final String driver = "com.mysql.jdbc.Driver";
	private static final String url = "jdbc:mysql://localhost:3306/post";
	private static final String user = "root";
	private static final String passwd = "213112";
	
	protected Connection conn;
	
	protected void init() throws DaoException{
		
		try {
			// データベースドライバの読込
			Class.forName(driver);
			conn = DriverManager.getConnection(url, user, passwd);
			
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
			throw new DaoException();
		}catch(SQLException e) {
			e.printStackTrace();
			throw new DaoException();
		}
		
	}
	
	protected void destroy() throws DaoException{
		try {
			if(conn != null) {
				conn.close();
			}
		}catch(SQLException e) {
			e.printStackTrace();
			throw new DaoException();
		}
	}
	

}
