package com.moonfield.postcode.postcode;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.moonfield.postcode.postcode.dao.PostCodeDao;
import com.moonfield.postcode.postcode.dto.PostCodeDto;

/**
 * Hello world!
 *
 */
public class Main {
	
    public static void main(String[] args){
    	
    	FileInputStream fi = null;
    	InputStreamReader is = null;
    	BufferedReader br = null;
    	
    	try {
    		
    		fi = new FileInputStream("KEN_ALL.CSV");
    		is = new InputStreamReader(fi);
    		br = new BufferedReader(is);
        	
    		Map<String, PostCodeDto> map = new HashMap<String, PostCodeDto>();
    		
        	String line;
        	while((line = br.readLine()) != null) {
        		
        		String[] arr = line.split(",");
        		
        		// 「以下に掲載がない場合」のときは文字列は空にする
        		if(arr[8].startsWith("\"以下に掲載がない場合\"")) {
        			
        			arr[8] = "";
        			arr[5] = "";
        			
        		}
        		
        		// 丁目の指定がある場合はその部分だけ削除する
        		Pattern p = Pattern.compile("（.+）");
        		Matcher m = p.matcher(arr[8]);
        		
        		if(m.find()) {
        			
        			arr[8] = arr[8].replaceFirst("（.+）", "");
        			arr[5] = arr[5].replaceFirst("\\(.+\\)", "");
        			
        		}
        		
        		// Dtoに値を詰める
        		PostCodeDto dto = new PostCodeDto();
        		dto.setPostcode(arr[2].replace("\"", ""));
        		dto.setPrefecture(arr[6].replace("\"", ""));
        		dto.setCity(arr[7].replace("\"", ""));
        		dto.setAddress(arr[8].replace("\"", ""));
        		dto.setPrefectureKana(arr[3].replace("\"", ""));
        		dto.setCityKana(arr[4].replace("\"", ""));
        		dto.setAddressKana(arr[5].replace("\"", ""));
        		
        		if(map.containsKey(dto.getPostcode())) {
        			
        			System.out.println("postcode:" + dto.getPostcode());
        			
        			PostCodeDto dto2 = map.get(dto.getPostcode());
        			
        			if(!dto.getAddress().equals(dto2.getAddress())) {
        				dto2.setAddress("");
        				dto2.setAddressKana("");
        				map.put(dto.getPostcode(), dto2);
        			}
        			
        		}else {
        			map.put(dto.getPostcode(), dto);
        		}
        		
        	}
        	
        	// データをインサートする
        	PostCodeDao dao = new PostCodeDao();
        	for(String key : map.keySet()) {
        		
        		PostCodeDto dto = map.get(key);
        		
        		boolean bool = false;
        		
        		while(!bool) {
        			
        			try {
        				int val = dao.insertPrefCode(dto);
        				
        				if(val == 1) {
        					bool = true;
        				}
        				
        			}catch(Exception e) {
        				
        			}
        		}
        	}
        	
    	}catch(Exception e) {
    		e.printStackTrace();
    	}
    	
    }
}
